<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Comment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $posts = Post::with('comments')->orderBy('created_at', 'desc')->take(10)->get();

        return View('home')->with('posts', $posts);

    }
    public function getNewPost(){

        return view('new_post');
    }

    public function getComment($id){
        $post =  Post::find($id);
        

        return view('new_comment',['post' =>$post->post,'post_id'=>$post->id ] );
    }


    public function postComment(request $request){
       
        $post = Comment::create(array(
            'comment' => Input::get('comment'),
            'post_id'=>Input::get('post_id'),
            'user_id'=>auth()->id()
        ));
        return redirect()->route('home')->with('status','Comment has been Published');

        

    }


    public function postNewPost(request $request){

        $post = Post::create(array(
            'post' => Input::get('post'),
            'user_id'=>auth()->id()
        ));
        return redirect()->route('home')->with('status','Post has been Published');
    }
}
