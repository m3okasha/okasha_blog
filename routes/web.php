<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/new/post','HomeController@getNewPost')->name('new.post');
Route::post('/new/post','HomeController@postNewPost')->name('new.post');

Route::get('/{id}/comment','HomeController@getComment')->name('new.comment');
Route::post('/new/comment','HomeController@postComment')->name('new.comment');