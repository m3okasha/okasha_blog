@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif



                    You are logged in!
                </div>
                <div class="card-body">
                <a href="{{route('new.post')}}">
                <button type="submit" class="btn">New Post</button>
                </a>
                </div>
            </div>
            <div class="card">

            @foreach($posts as $post)

              <div class="card-body">
              <font size="+3"> <strong  >{{$post->post }}</strong>
              </font>
              <ul>
              @foreach($post->comments as $comment)
            
               <li> {{$comment->comment}} </li>
                
               @endforeach
               </ul>
              
              <br>
            <a href="{{$post->id}}/comment"> Comment </a>
            </div>
            @endforeach


            </div>
        </div>
    </div>
</div>
@endsection
