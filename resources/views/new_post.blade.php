@extends('layouts.app')

@section('content')
<div class="container">
  <h1> New Post </h1>
  <form method="post" action="{{route('new.post')}}"  >
  
  {{ csrf_field() }}

  	<div class="form-group">
  		<label for="post"> New Post </label>
<textarea  class="form-control" id="id_post" name="post" placeholder="post">
</textarea>
  
		</div>
		<button type="submit" >Post</button>
  </form>
</div>
@endsection
